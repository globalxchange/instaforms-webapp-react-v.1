import React,{useState} from 'react';
import './postregistersidebar.scss';
import plus from './images/plus.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

function PostregisterSidebar() {
    const [category, setcategory] = useState(false);
    return (
        <>
        <div className="container">
        <div className="sidebar">
        <div className="heading-sec">
        <div className="heading">
            <h3>Project Board</h3>
        </div>
        <div className="heading-btn">
            <button className="newbtn">
            <img src={plus} className="plus-img" alt="img"/>
            </button>
         </div>
        </div>
        <hr style={{border: "0.5px solid #EFEFEF"}}/>
        <div className="dropdown">
        <div className="dropdown-left">
          <h3>No Projects yet</h3> 
          </div>
          <div className="dropdown-right"  onClick={() => {
                setcategory(!category);
              }}>
         <FontAwesomeIcon icon={category ? faChevronUp : faChevronDown} />
         </div>
        </div>
        {category ? (
              <div>

              </div>
            ) : (
              ''
            )}
        </div>  
        </div>
        </>
    )
}

export default PostregisterSidebar
