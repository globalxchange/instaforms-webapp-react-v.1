import React,{useState} from 'react';
import './dashboardsidebar.scss';
import plus from './images/plus.svg';
import dotone from './images/dotsone.svg';
import check2 from './images/check2.png';
import dottwo from './images/dots-two.svg';
import pin from './images/pin.svg';
import pin2 from './images/pin2.svg';
import check from './images/check.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';


function DashboardSidebar() {
  const [category, setcategory] = useState(true);
    
    return (
        <>
        <div className="container">
        <div className="sidebar">
        <div className="heading-sec">
        <div className="heading">
            <h3>Project Board</h3>
        </div>
        <div className="heading-btn">
            <button className="newbtn">
            <img src={plus} className="plus-img" alt="img"/>
            </button>
         </div>
        </div>
        <hr style={{border: "0.5px solid #EFEFEF"}}/>
        <div className="dropdown">
        <div className="dropdown-left">
          <h3>UI/X Design Project</h3> 
          </div>
          <div className="dropdown-right"  onClick={() => {
                setcategory(!category);
              }}>
         <FontAwesomeIcon icon={category ? faChevronUp : faChevronDown} />
         </div>
        </div>
        {category ? (
              <div className="drpdwn-itms">
                  <div className="itm-one">
                  <div className="card-top1">
                  <div className="top-left1">
                  <p className="tagline1">Toptal</p>
                  </div>
                  <div className="top-middle1"></div>
                  <div className="top-right1">
                  <img src={dotone} className="dotoneimg1" alt="img"/>
                  </div>
                  </div>
                  <div className="card-middle1">
                  <p className="card-middle-head1">Duito App redesign</p>
                  <p className="card-middle-txt1">Mobile app design for finance wallet app</p>
                  </div>
                  <div className="card-bottom1">
                  <div className="bottom-left1">
                  <div className="bottom-img1one-div">
                  <div className="bottom-img1one">
                  </div>
                  </div>
                  <div className="bottom-img2one-div">
                  <div className="bottom-img2one">
                  </div>
                  </div>
                  </div>
                  <div className="bottom-right1">
                  <img src={check} className="bottom-img3one" alt="img"/>&nbsp;2/4&nbsp;
                  <img src={pin} className="bottom-img4one" alt="img"/>&nbsp;4
                  </div>
                  </div>
                  </div>
                  <div className="itm-two">
                  <div className="card-top2">
                  <div className="top-left2">
                  <p className="tagline2">Freelancer</p>
                  </div>
                  <div className="top-middle2"></div>
                  <div className="top-right2">
                  <img src={dottwo} className="dotoneimg2" alt="img"/>
                  </div>
                  </div>
                  <div className="card-middle2">
                  <p className="card-middle-head2">Landing Page Wallet.io</p>
                  <p className="card-middle-txt2">Landing Page redesign for wallet.io</p>
                  </div>
                  <div className="card-bottom2">
                  <div className="bottom-left2">
                  <div className="bottom-img1two-div">
                  <div className="bottom-img1two">
                  </div>
                  </div>
                  <div className="bottom-img2two-div"> 
                  <div className="bottom-img2two">
                  </div>
                  </div>
                  <div className="bottom-img3two-div">
                  <div className="bottom-img3two">
                  </div>
                  </div>
                  </div>
                  <div className="bottom-middle2"></div>
                  <div className="bottom-right2">
                  <img src={check2} className="bottom-img3two" alt="img"/>&nbsp;4/6&nbsp;
                  <img src={pin2} className="bottom-img4two" alt="img"/>&nbsp;2
                  </div>
                  </div>
                  </div>
                  <div className="itm-three">
                  <div className="card-top3">
                  <div className="top-left3">
                  <p className="tagline3">Freelancer</p>
                  </div>
                  <div className="top-middle3"></div>
                  <div className="top-right3">
                  <img src={dotone} className="dotoneimg3" alt="img"/>
                  </div>
                  </div>
                  <div className="card-middle3">
                  <p className="card-middle-head3">Scooter App Design</p>
                  <p className="card-middle-txt3">Scooter Mobile App for Jakarta city</p>
                  </div>
                  <div className="card-bottom3">
                  <div className="bottom-left3">
                  <div className="bottom-img1three-div">
                  <div className="bottom-img1three">
                  </div>
                  </div>
                  <div className="bottom-img2three-div">
                  <div className="bottom-img2three">
                  </div>
                  </div>
                  </div>
                  <div className="bottom-middle3"></div>
                  <div className="bottom-right3">
                  <img src={check} className="bottom-img3three" alt="img"/>&nbsp;4/8&nbsp;
                  <img src={pin} className="bottom-img4three" alt="img"/>&nbsp;8
                  </div>
                  </div>
                  </div>
                  {/* <div className="itm-four"> */}
                  {/* <div className="card-top4">
                  <div className="top-left4">
                  <p className="tagline4">Freelancer</p>
                  </div>
                  <div className="top-middle4"></div>
                  <div className="top-right4">
                  <img src={dotone} className="dotoneimg4"/>
                  </div>
                  </div>
                  <div className="card-middle4">
                  <p className="card-middle-head4">Scooter App Design</p>
                  <p className="card-middle-txt4">Scooter Mobile App for Jakarta city</p>
                  </div>
                  <div className="card-bottom4">
                  <div className="bottom-left4">
                  <div className="bottom-img1four">
                  </div>
                  <div className="bottom-img2four">
                  </div>
                  </div>
                  <div className="bottom-middle4"></div>
                  <div className="bottom-right4">
                  <img src={check} className="bottom-img3four"/>&nbsp;4/8&nbsp;
                  <img src={pin} className="bottom-img4four"/>&nbsp;8
                  </div>
                  </div> */}
                  {/* </div> */}
              
              </div>
            ) : (
              ''
            )}
        </div>  
        </div>
        </>
    )
}

export default DashboardSidebar
