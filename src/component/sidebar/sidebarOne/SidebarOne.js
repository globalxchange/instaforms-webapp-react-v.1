import React, { useState } from 'react';
import './sidebarone.scss';
import formsimg2 from './images/forms2.svg';

function SidebarOne() {

    const [selected1, setSelected1] = useState({ name: '' });
    const [selected2, setSelected2] = useState({ name: '' });
    // const [subform, setSubform] = useState(0);




    const Items = [
        {
            id: 1,
            name: "Forms",
            image2: formsimg2

        },
        {
            id: 2,
            name: "Campaign",
            image2: formsimg2
        },
        {
            id: 3,
            name: "APIMachine",
            image2: formsimg2
        },
        {
            id: 4,
            name: "Contact",
            image2: formsimg2
        }
    ]

    const SubItems = [
        {
            id: 1,
            name: "Blank",
            image2: formsimg2
        },
        {
            id: 2,
            name: "Templates",
            image2: formsimg2
        },
        {
            id: 3,
            name: "Custom",
            image2: formsimg2
        },
        {
            id: 4,
            name: "Code",
            image2: formsimg2
        }
    ]
    console.log("dsfsfsd", selected1.name)

    return (
        <div className="container">
            <div className="sidebarone" >
                <div className="content-items">
                    {Items.map((x, i) => {
                        return (
                            <div onClick={() => selected1.name !== '' ? setSelected2({ name: '' }) || setSelected1({ name: '' }) : (i === 0) ? setSelected1(x) : ''} className="content1" style={selected1.name === x.name ? { border: "0.5px  #E5E5E5", borderBottom: "0.5px solid #E5E5E5" } : (selected1.name === '') ? { border: "0.5px  #E5E5E5", borderBottom: "0.5px solid #E5E5E5" } : { borderBottom: 0 }}>
                                <div style={selected1.name === x.name ? { color: '#000000', opacity: 2, display: "flex" } : {}}
                                    className={(selected1.name !== "") ? "txt-content-none" : "txt-content"}>
                                    <img src={x.image2}  alt="img"/> &nbsp;
                            <h5 className="item-name">{x.name}</h5>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <div className="sidebartwo" >
                <div className="content-items">
                    {SubItems.map(x => {
                        return (
                            <div onClick={() => setSelected2(x)} className={selected1.name === '' ? "content2none" : "content2"} style={selected2.name === x.name ? { border: "0.5px  #E5E5E5", borderBottom: "0.5px solid #E5E5E5" } : (selected2.name === '') ? { border: "0.5px  #E5E5E5", borderBottom: "0.5px solid #E5E5E5" } : { borderBottom: 0 }}>
                                <div style={selected2.name === x.name ? { color: '#000000', opacity: 2, display: "flex" } : {}}
                                    className={(selected2.name !== "") ? "txt-content-none" : "txt-content"}>
                                    <img src={x.image2} alt="img"/> &nbsp;
                              <h5 className="item-name">{x.name}</h5>

                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}

export default SidebarOne
