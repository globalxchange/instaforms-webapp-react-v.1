import React, { useState } from 'react';
import './homeNavbar.scss';
import logo from '../../../images/Logo.svg';
import logombl from '../../../images/logombl.png';
import hamburger from '../../../styles/hamburger.scss';


function HomeNavbar() {
    const [navOpen, setNavOpen] = useState(false);

    const loginpage = () => {
        window.location.href = 'login';
    }

    return (
        <>
            <div className= "home-section1-navbar">
                <div className="navbar-left">
                    <img className="logoimg" src={logo} alt="No img"></img>
                </div>
                <div className="navbar-right">
                    <a href="/">Features</a>
                    <a href="/templates" className={` ${window.location.pathname === '/templates' ? "active" : ''}`}>Templates</a>
                    <a href="/">Mobile</a>
                    <a href="/">Pricing</a>
                    <button className="navbar-right-btnone" onClick={loginpage}>Login</button>
                    <button className="navbar-right-btntwo">Get Started</button>
                </div>
            </div>
            <div className={navOpen ? 'home-section1-navbarmbl' : 'home-section1-navbarmblnone'}>
                <div className="navbar-cross">
                    <div
                        onClick={() => {
                            setNavOpen(!navOpen)
                        }}
                        className={
                            'd-flex hamburger hamburger--squeeze' + (navOpen ? ' is-active' : '')
                        }
                    >
                        <span class="hamburger-box m-auto">
                            <span class="hamburger-inner"></span>
                        </span>
                    </div>
                </div>
                <div className="logoimgmbl">
                    <img src={logombl} alt="No img"></img>
                </div>
                <div className="navbar-rightmbl">
                    <a href="/">Features</a>
                    <a href="/templates">Templates</a>
                    <a href="/">Mobile</a>
                    <a href="/">Pricing</a>
                    <button className="navbar-right-btnonembl" onClick={loginpage}>Login</button>
                    <button className="navbar-right-btntwombl">Get Started</button>
                </div>
            </div>
            <div className={navOpen ? 'home-section1-navbarmblnone'  : 'home-section1-navbarmbltwo'}>
                <div className="navbar-crosstwo">
                    <div
                        onClick={() => {
                            setNavOpen(!navOpen)
                        }}
                        className={
                            'd-flex hamburger hamburger--squeeze' + (navOpen ? ' is-active' : '')
                        }
                    >
                        <span class="hamburger-box m-auto">
                            <span class="hamburger-inner"></span>
                        </span>
                    </div>
                </div>
                <img className="logoimgmbltwo" src={logombl} alt="No img"></img>
            </div>
        </>
    )
}

export default HomeNavbar

