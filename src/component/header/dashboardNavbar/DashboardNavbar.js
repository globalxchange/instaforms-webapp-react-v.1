import React from 'react';
import './dashboardnavbar.scss';
import { useHistory } from "react-router-dom";
import logo from '../../../images/Instaformslogo2.svg';
import plus from '../../../images/plus.svg';

function DashboardNavbar() {
    const history = useHistory();
    return (
               <div className= "navbar-container">
                <div className="navbar-left">
                    <img className="logoimg" src={logo} alt="No img"></img>
                </div>
                <div className="navbar-right">
                    <a href="/dashboard" className={` ${window.location.pathname === '/dashboard' ? "active" : "menu-items"}`}>Dashboard</a>
                    <a href="/market" className={` ${window.location.pathname === '/market' ? "active" : "menu-items"}`}>Market</a>
                    <a href="/data" className={` ${window.location.pathname === '/data' ? "active" : "menu-items"}`} style={{marginLeft:"-3%"}}>Data</a>
                    <a href="/integrations" className={` ${window.location.pathname === '/integrations' ? "active" : "menu-items"}`} style={{marginLeft:"-2%"}}>Integrations</a>
                    <button className="navbar-btn" onClick={() => history.push('/dashboard/createnew')}>
                    <img src={plus} className="plus-img" alt="img"/>&nbsp;
                    New
                    </button>
                </div>
            </div>
    )
}

export default DashboardNavbar
