import React, { useState } from 'react';
import './postregistration.scss';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';
import Lottie from 'react-lottie';
import animationData from '../login/paperpen.json';
import { useHistory } from 'react-router-dom';
import userImg from './images/userimage.svg';
import submitLogo from './images/submitlogo.svg';
import RegisterNavbar from '../../header/registerNavbar/RegisterNavbar';
import PostregisterSidebar from '../../sidebar/dashboard/PostregisterSidebar';


function PostRegistration() {

    const history = useHistory();
    const [picture, setPicture] = useState("");
    const [username, setUsername] = useState("");
    const [loading, setLoading] = useState(false);
    let email = localStorage.getItem('userEmail');
    let name = email.substring(0, email.lastIndexOf("@"));

    let token = localStorage.getItem('Tokenid');

    const register = () => {
        setLoading(true)
        const headers = {
            'Content-Type': 'application/json',
            'Accept': '*/*',
            'email': email,
            'token': token
        }

        axios.post('https://appapi2.apimachine.com/user/register', {
            headers: headers,
            body: ({
                "name": username,
                "profilePicURL": picture
            })
        })
            .then((response) => {
                setLoading(false);
                if (response.data.status) {
                    history.replace("/dashboard");
                }
            })
            .catch((error) => {
                setLoading(false);
                toast.error("error", error);
            })
    }

    const onChangeUsername = e => {
        setUsername(e.target.value);
    }

    const onChangePicture = e => {
        console.log('picture: ', e.target.value);
        setPicture(e.target.value);
    };


    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings:
            {
                preserveAspectRatio: 'xMidYMid slice'
            }
    }

    return (
        <>
            {loading ? (
                <React.Fragment >
                    <div className="loading-cont">
                        <Lottie height={450} width={450} options={defaultOptions} className="loading-img" />
                    </div>
                </React.Fragment>
            ) : (
                    <div>
                        <div className="main-navbar">
                            <RegisterNavbar />
                        </div>
                        <div className="dashboard-container">
                            <div className="leftside-container">
                                <PostregisterSidebar />
                            </div>
                            <div className="rightside-container">
                                <div className="headsec-div">
                                    <h1 className="heading">Welcome, {name}</h1>
                                    <p className="description">This Is Your InstaForms Dashboard. Here You Can Build, Deploy, & Manage Your Forms. In order to start using your InstaForms account, you have to complete you profile registration.</p>
                                </div>
                                <div className="forms-div">
                                    <div className="form-step1">
                                        <label className="step1-label">Step 1: Create A InstaForms Username</label><br />
                                        <input className="step1-input" placeholder="ex. shorupan" type="text" name="username" value={username} onChange={onChangeUsername} />
                                    </div>
                                    <div className="form-step2">
                                        <label className="step2-label">Step 2: Add Profile Photo</label><br />
                                        <div className="step2-input-container">
                                            <label className="step2-label2" >
                                                <input className="step2-input" type="file" name="photo" onChange={onChangePicture} />
                                                {picture ? picture : "Upload A Photo"}  </label>
                                            <img src={userImg} alt="Noimg" className="user-icon" />
                                            <div className="icon-background">
                                                <img src={submitLogo} className="submitbtn-icon" alt="Noimg" onClick={register} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ToastContainer />
                    </div>
                )}

        </>
    )
}

export default PostRegistration
