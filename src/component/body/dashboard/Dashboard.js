import React, { useState, useEffect } from "react";
import "./dashboard.scss";
import { useHistory } from "react-router-dom";
import { authenticate } from "../../../services/postAPIs";
import cloud from "./images/cloud.svg";
import share from "./images/share.svg";
import dots from "./images/dots-two.svg";
import DashboardNavbar from "../../header/dashboardNavbar/DashboardNavbar";
import DashboardSidebar from "../../sidebar/dashboard/DashboardSidebar";

function Dashboard() {
  const history = useHistory();

  const [initialValue, setInitialValue] = useState("January 2019");
  const [selected, setSelected] = useState();
  const [checkbox1, setCheckbox1] = useState(true);
  const [checkbox2, setCheckbox2] = useState(true);
  const [checkbox3, setCheckbox3] = useState(false);
  const [checkbox4, setCheckbox4] = useState(false);
  const [safe, setSafe] = useState(true);
  const [days, setDays] = useState([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,]);

  useEffect(() => {
    authenticate().then((res) => {
      if (res.data.status) {
        setSafe(true);
      } else {
        setSafe(false);
        history.replace("/login");
      }
    });

    var d = new Date();
    var n = d.getDate();
    setSelected(n);
  }, []);

  const handleAddrTypeChange = (e) => {
    const d = [];
    for (let i = 0; i <= e.target.value; i++) {
      d.push(i);
    }
    setDays(d);
  };

  const handleChange = () => {
    setCheckbox1(!checkbox1);
  };

  const handleChange1 = () => {
    setCheckbox2(!checkbox2);
  };
  const handleChange2 = () => {
    setCheckbox3(!checkbox3);
  };
  const handleChange3 = () => {
    setCheckbox4(!checkbox4);
  };
  return (
    <div>
      <div className="main-navbar">
        <DashboardNavbar />
      </div>
      <div className="dashboard-content">
        <div className="leftside-container">
          <DashboardSidebar />
        </div>
        <div className="rightside-container">
          <div className="container">
            <div className="head-sec">
              <div className="heading">
                <h3>Project Roadmap</h3>
              </div>
              <div className="middle"></div>
              <div className="head-right">
                <div className="icon-one">
                  <img src={cloud} className="img-one" alt="img" />
                </div>
                <div className="icon-two">
                  <img src={share} className="img-two" alt="img" />
                </div>
                <div className="dropdown">
                  <select
                    defaultValue={initialValue}
                    onChange={handleAddrTypeChange}
                    className="dropdown-box"
                  >
                    <option value="31">
                      January 2019
                    </option>
                    <option value="28">february 2019</option>
                    <option value="31">March 2019</option>
                    <option value="30">April 2019</option>
                  </select>
                </div>
                <div></div>
              </div>
            </div>
            <div className="calender-content">
              <div className="calender-items">
                <div className="numbers-div">
                  {days.map((numb, i) => {
                    return (
                      <p
                        key={i}
                        onClick={() => {
                          setSelected(numb);
                        }}
                        className={
                          selected === numb ? "selectednumbers" : "numbers"
                        }
                      >
                        {numb}
                      </p>
                    );
                  })}
                </div>
                <div className="numbers-body">
                  {days.map((numb, i) => {
                    return <p key={i}></p>;
                  })}
                  <div className="progress-container">
                    <div className="progress-content">
                      <div className="progress">
                        <div className="bar" style={{ width: "65%" }}>
                          <div className="bar-txt">
                            <p className="inside-txt">
                              Wireframing for Duito Mobile App
                            </p>
                          </div>
                          <div className="img1" alt="img">
                            {" "}
                          </div>
                          <div className="img2" alt="img">
                            {" "}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="progress-content2">
                      <div className="progress2">
                        <div className="bar2" style={{ width: "65%" }}>
                          <div className="bar2-txt">
                            <p className="inside-txt2">
                              High-Fidelity for Duito Mobile App
                            </p>
                          </div>
                          <div className="img3" alt="img">
                            {" "}
                          </div>
                          <div className="img4" alt="img">
                            {" "}
                          </div>
                          <div className="img5" alt="img">
                            {" "}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="progress-content3">
                      <div className="progress3">
                        <div className="bar3" style={{ width: "65%" }}>
                          <div className="bar3-txt">
                            <p className="inside-txt3">
                              Project Requirements Scooter App
                            </p>
                          </div>
                          <div className="img6" alt="img">
                            {" "}
                          </div>
                          <div className="img7" alt="img">
                            {" "}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="progress-content4">
                      <div className="progress4">
                        <div className="bar4" style={{ width: "65%" }}>
                          <div className="bar4-txt">
                            <p className="inside-txt4">
                              Sketching & Wireframing Foodly
                            </p>
                          </div>
                          <div className="img8" alt="img">
                            {" "}
                          </div>
                          <div className="img9" alt="img">
                            {" "}
                          </div>
                          <div className="img10" alt="img">
                            {" "}
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="progress-content5">
                      <div className="progress5">
                        <div className="bar5" style={{ width: "65%" }}>
                          <div className="bar5-txt">
                            <p className="inside-txt5">
                              Define a Problem Crypto Wallet
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className="progress6">
                        <div className="bar6" style={{ width: "65%" }}>
                          <div className="bar6-txt">
                            <p className="inside-txt6">
                              Sketching & Wireframing Foodly
                            </p>
                          </div>
                          <div className="img11" alt="img">
                            {" "}
                          </div>
                          <div className="img12" alt="img">
                            {" "}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="calender-card-div">
                  <div className="calender-card-left"></div>
                  <div className="calender-card-right">
                    <div className="calender-card">
                      <div className="card-content">
                        <div className="card-head">
                          <div className="cardhead-left">
                            <p className="left-txt">Wireframing Wallet.io</p>
                          </div>
                          <div className="cardhead-right">
                            <img src={dots} alt="img" />
                          </div>
                        </div>
                        <div className="line">
                          <hr />
                        </div>
                        <div className="card-middle">
                          <div className="card-middle-div">
                            <div className="card-middle-txt">
                              <p className="card-middle-p">Task Project</p>
                            </div>
                            <div style={{ width: "30%", height: "25%" }}></div>
                            <div className="progress-txt">
                              <h5>50%</h5>
                            </div>
                          </div>

                          <div className="card-middle-progress">
                            <progress
                              className="progress"
                              id="file"
                              value="50"
                              max="100"
                            >
                              {" "}
                            </progress>
                          </div>
                        </div>
                        <div className="card-bottom">
                          <div className="line-one">
                            <div className="checkbox1">
                              <input
                                type="checkbox"
                                checked={checkbox1}
                                onChange={handleChange}
                              />
                            </div>
                            {checkbox1 ? (
                              <div className="checkbox1-text1">
                                <h5>
                                  <del>Section Hero Wallet.io</del>
                                </h5>
                              </div>
                            ) : (
                              <div className="checkbox1-text2">
                                <h5>Section Hero Wallet.io</h5>
                              </div>
                            )}
                          </div>
                          <div className="line-two">
                            <div className="checkbox2">
                              <input
                                type="checkbox"
                                checked={checkbox2}
                                onChange={handleChange1}
                              />
                            </div>
                            {checkbox2 ? (
                              <div className="checkbox2-text1">
                                <h5>
                                  <del>Section Features Wallet.io</del>
                                </h5>
                              </div>
                            ) : (
                              <div className="checkbox2-text2">
                                <h5>Section Features Wallet.io</h5>
                              </div>
                            )}
                          </div>
                          <div className="line-three">
                            <div className="checkbox3">
                              <input
                                type="checkbox"
                                checked={checkbox3}
                                onChange={handleChange2}
                              />
                            </div>
                            {checkbox3 ? (
                              <div className="checkbox3-text1">
                                <h5>
                                  <del>Section Services wallet.io</del>
                                </h5>
                              </div>
                            ) : (
                              <div className="checkbox3-text2">
                                <h5>Section Services wallet.io</h5>
                              </div>
                            )}
                          </div>

                          <div className="line-four">
                            <div className="checkbox4">
                              <input
                                type="checkbox"
                                checked={checkbox4}
                                onChange={handleChange3}
                              />
                            </div>
                            {checkbox4 ? (
                              <div className="checkbox4-text1">
                                <h5>
                                  <del>Section Services wallet.io</del>
                                </h5>
                              </div>
                            ) : (
                              <div className="checkbox4-text2">
                                <h5>Section Services wallet.io</h5>
                              </div>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
