import React, { Component } from 'react';
import "./hometwo.scss";
import HomeNavbar from '../../header/homeNavbar/HomeNavbar';
import google from '../../../images/google.png';
import fb from '../../../images/fb.png';
import ln from '../../../images/linkedin.jpg';
import ms from '../../../images/msoffice.png';
import banner2 from '../../../images/banner2.png';
import banner3 from '../../../images/banner3.png';
import banner4 from '../../../images/banner4.png';
import banner5 from '../../../images/banner5.png';
import banner6 from '../../../images/banner6.png';
import sheet from '../../../images/sheet-logo.png';
import crmlogo from '../../../images/crm-logo.png';
import salesforce from '../../../images/salesforce.png';
import zohosign from '../../../images/zoho-sign.png';
import campaign from '../../../images/campaigns-logo.png';
import projects from '../../../images/zoho-projects.png';
import desk from '../../../images/zoho-desk.webp';
import zapier from '../../../images/download.png';
import writer from '../../../images/Zoho_Writer.png';
import drive from '../../../images/drive.png';
import gplay from '../../../images/gplay.png';
import appstore from '../../../images/appstore.png';
import setting from '../../../images/setting.png';
import mobile from '../../../images/mobile.png';
import privacy from '../../../images/privacy.png';
import capterra from '../../../images/capterra.png';
import trust from '../../../images/trust.png';
import getapp from '../../../images/getapp.png';
import gcrowd from '../../../images/gcrowd.png';
import build from '../../../images/build.png';

export class HomeTwo extends Component {

    constructor(props) {
        super(props)

        this.state = {
            list: [
                {
                    id: 1,
                    image: "https://cdn.pixabay.com/photo/2016/04/21/01/31/handsome-1342457_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "We've converted several paper-based processes from paper to online — all thanks to Zoho Forms! Most of our customers ask, Can we do this online Zoho Forms was our answer to them.",
                    name: "Conan Power",
                    designation: "Information Systems Analyst, Waterford City Council"
                },
                {
                    id: 2,
                    image: "https://cdn.pixabay.com/photo/2016/10/07/12/19/ipad-1721428_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Our company needed a tool like Zoho Forms to help generate leads. Easy embedding of forms onto our website and seamless tracking using the Adwords integration made Zoho Forms the perfect fit for our business",
                    name: "Aswini Srinivasan",
                    designation: "CEO, Headphone Zone"
                },
                {
                    id: 3,
                    image: "https://cdn.pixabay.com/photo/2017/05/31/08/37/single-2359491_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Zoho Forms makes collecting and managing data incredibly simple. It saves a lot of time and is worth every penny.",
                    name: "Shannon Marie Baker",
                    designation: "Co-founder, Inspire Productions LLC"
                },
                {
                    id: 4,
                    image: "https://cdn.pixabay.com/photo/2017/08/15/16/28/portrait-2644622_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Our company needed a tool like Zoho Forms to help generate leads. Easy embedding of forms onto our website and seamless tracking using the Adwords integration made Zoho Forms the perfect fit for our business.",
                    name: "Raghav Soman",
                    designation: "Co-founder, 80 Degrees East"
                },
                {
                    id: 5,
                    image: "https://cdn.pixabay.com/photo/2014/07/12/20/12/man-391413_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Keeping Digital India in mind, Zoho Forms is playing a prominent role. It's an extraordinary concept that offers good service and a one stop solution for any requirement.",
                    name: "Pranesh Padmanabhan",
                    designation: "CEO, Studio 31."
                },
                {
                    id: 6,
                    image: "https://cdn.pixabay.com/photo/2017/07/22/20/40/portrait-2529905_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Zoho Forms drives everything we do, from following up with clients and prospects, to keeping detailed notes on all of our activity. It's fantastic and easy to use!",
                    name: "Nanda Kishore",
                    designation: "Team Manager"
                },
                {
                    id: 7,
                    image: "https://cdn.pixabay.com/photo/2018/03/17/08/52/portrait-3233413_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Zoho Forms drives everything we do, from following up with clients and prospects, to keeping detailed notes on all of our activity. It's fantastic and easy to use!",
                    name: "Shannon Marie Baker",
                    designation: "CEO, Headphone Zone"
                },
                {
                    id: 8,
                    image: "https://cdn.pixabay.com/photo/2017/12/27/21/23/folk-3043873_960_720.jpg",
                    heading: "New success stories everyday",
                    content: "Zoho Forms drives everything we do, It's an extraordinary concept that offers good service and to keeping detailed notes on all of our activity. It's fantastic and easy to use!",
                    name: "John Whaling",
                    designation: "CEO, Studio 31"
                }
            ],
            selected: {
                id: 8,
                image: "https://cdn.pixabay.com/photo/2017/12/27/21/23/folk-3043873_960_720.jpg",
                heading: "New success stories everyday",
                content: "Zoho Forms drives everything we do, It's an extraordinary concept that offers good service and to keeping detailed notes on all of our activity. It's fantastic and easy to use!",
                name: "John Whaling",
                designation: "CEO, Studio 31"
            }
        };
    }


    render() {
        return (
            <div className="home">
                <div className="home-section1">
                    <HomeNavbar />
                    <div className="banner1">
                        <div className="banner1-left">
                            <p className="banner1-left-p1">Let's Be Honest</p>
                            <p className="banner1-left-p2">Forms Suck</p>
                        </div>
                        <div className="banner1-right">
                            <p className="banner1-right-p1">Get started with our free online form builder.</p>
                            <div className="banner1-right-form">
                                <input className="banner1-right-input" type="text" placeholder="Company Name" />
                                <input className="banner1-right-input" type="text" placeholder="Email" />
                                <input className="banner1-right-input" type="text" placeholder="Password" />
                                <input className="banner1-right-input" type="text" placeholder="Phone Number" />
                            </div>
                            <p className="banner1-right-p2">Based on your IP, you are in India.<span className="banner1-right-p2-span">Change</span><br />Your data will be in INDIA data center.</p>
                            <input type="checkbox" className="checkbox" /><span className="checkbox-text"> I agree to the <span className="banner1-right-p2-span">Terms of Service</span> and <span className="banner1-right-p2-span">Privacy Policy.</span></span><br />
                            <button className="banner1-right-button">SIGN UP FOR FREE</button><br />
                            <p className="bannerright-p">or sign in using <span><img className="gimg" src={google} alt="No img"></img></span><span><img className="gimg" src={fb} alt="No img"></img></span><span><img className="gimg" src={ln} alt="No img"></img></span><span><img className="gimg" src={ms} alt="No img"></img></span></p>
                        </div>
                        <div>
                        </div>
                    </div>
                    <div className="banner2">
                        <img className="banner2-img" src={banner2} alt="No img"></img>
                    </div>
                    <div className="banner3">
                        <div className="banner3-left">
                            <span className="label-banner3-left">CREATE</span>
                            <h1 className="h-banner3-left">Create beautiful forms without having to code</h1>
                            <p className="p-banner3-left">Our feature-rich form builder software, with 40+ field types, customizable themes, situation-specific templates, and a simple user interface, helps you create beautiful, functional forms for all your needs</p>
                            <p className="p1-banner3-left">Explore the form builder software &nbsp; &#8594; </p>
                        </div>
                        <div className="banner3-right">
                            <img className="banner3-img" src={banner3} alt="No img"></img>
                        </div>
                    </div>
                    <div className="banner4">
                        <div className="banner4-left">
                            <img className="banner4-img" src={banner4} alt="No img"></img>
                        </div>
                        <div className="banner4-right">
                            <span className="label-banner4-right">SHARE</span>
                            <h1 className="h-banner4-right">Collect data from all corners</h1>
                            <p className="p-banner4-right">Embed forms on web pages and engage with your website visitors. Share links on social media or to a target audience via email campaigns. Let your forms reach a wider audience or keep them private within a closed organization.</p>
                            <p className="p1-banner4-right">Explore the form sharing options &nbsp; &#8594; </p>
                        </div>
                    </div>
                    <div className="banner5">
                        <div className="banner5-left">
                            <span className="label-banner5-left">NOTIFY</span>
                            <h1 className="h-banner5-left">Stay informed with instant notifications</h1>
                            <p className="p-banner5-left">Trigger conditional alerts via email whenever a new record is submitted or updated. Include attachments and keep all concerned people in the loop. Receive instant updates on chat platforms where your teams can work collaboratively. </p>
                            <p className="p1-banner5-left">Learn more about form notifications Previous &nbsp; &#8594; </p>
                        </div>
                        <div className="banner5-right">
                            <img className="banner5-img" src={banner5} alt="No img"></img>
                        </div>
                    </div>
                    <div className="banner6">
                        <div className="banner6-left">
                            <img className="banner6-img" src={banner6} alt="No img"></img>
                        </div>
                        <div className="banner6-right">
                            <span className="label-banner6-right">ANALYZE</span>
                            <h1 className="h-banner6-right">View, analyze, and optimize data</h1>
                            <p className="p-banner6-right">Sort and view form entries, export them as spreadsheets, or send the data to apps you use. With UTM tracking and form analytics, measure your form’s performance and optimize it for better conversions.</p>
                            <p className="p1-banner6-right">View form analytics in detail  &nbsp; &#8594; </p>
                        </div>
                    </div>
                    <div className="banner7">
                        <div className="banner7-one">
                            <h1 className="h-banner7">Let data flow to your daily apps<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;with form integrations</h1>
                            <p className="p-banner7">With our seamless integrations, Zoho Forms becomes a reliable front-end data collection <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; system that works compatibly with various applications to fuel all <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;kinds of businesses.</p>
                        </div>
                        <div className="banner7-two">
                            <div className="banner7-two-cards">
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={sheet} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Sheet</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={crmlogo} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Crm</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={salesforce} alt="No img"/>
                                    <p className="banner7-two-text">Salesforce</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={zohosign} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Sign</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={campaign} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Campaigns</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={projects} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Projects</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={drive} alt="No img"/>
                                    <p className="banner7-two-text">Google drive</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={desk} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Desk</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={zapier} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Sheet</p>
                                </div>
                                <div className="banner7-two-flex">
                                    <img className="banner7-two-img" src={writer} alt="No img"/>
                                    <p className="banner7-two-text">Zoho Sheet</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="banner8">
                        <div className="banner8-left">
                            <video className="banner8-vedio" autoPlay loop>
                                <source src="https://www.zohowebstatic.com/sites/default/files/forms/images/mob-slider.mp4" type="video/mp4" />
                            </video>
                        </div>
                        <div className="banner8-right">
                            <h1 className="h-banner8-right">Mobile forms for mobile teams</h1>
                            <p className="p-banner8-right">All the functionality of our online form builder  now accessible on any device. With our mobile apps for iOS and Android, you can create forms both online and offline, consolidate data, and collaborate with your team—all while on the go.<span style={{ color: "#39c5ea" }}> Learn more</span></p>
                            <img className="banner8-right-img1" src={gplay} alt="No img"/>
                            <img className="banner8-right-img2" src={appstore} alt="No img"/>
                        </div>
                    </div>
                    <div className="banner9">
                        <div className="banner9-left">
                            <h1 className="h-banner9-left">{this.state.selected.heading}</h1>
                             <p className="p-banner9-left">{this.state.selected.content}</p>
                             <p className="p1-banner9-left">{this.state.selected.name}</p>
                             <p className="p2-banner9-left">{this.state.selected.designation}</p>
                        </div>
                        <div className="banner9-right">
                        <div className="banner9-rightdiv1">
                        { this.state.list.slice(0, 2).map(x => <img onClick = {()=>this.setState({selected : x})} className={this.state.selected.image===x.image?'banner9-img-selected':"banner9-img"} src={x.image} alt="No img"></img>)}
                       </div>
                       <div className="banner9-rightdiv2">
                        { this.state.list.slice(2, 5).map(x => <img onClick = {()=>this.setState({selected : x})} className={this.state.selected.image===x.image?'banner9-img-selected':"banner9-img"} src={x.image} alt="No img"></img>)}
                       </div>
                       <div className="banner9-rightdiv3">
                        { this.state.list.slice(5, 8).map(x => <img onClick = {()=>this.setState({selected : x})} className={this.state.selected.image===x.image?'banner9-img-selected':"banner9-img"} src={x.image} alt="No img"></img>)}
                       </div>
                        </div>
                    </div>
                    <div className="banner10">
                     <div className="banner10-cards">
                     <div className="banner10-flex1">
                       <img className="banner10-img" src={setting} alt="No img"/>
                       <h2 className="banner10-heading">Collaborate and automate</h2>
                       <p className="banner10-p">Set up an approval hierarchy and automate task assignment. Bring your team on board to review, edit, and collaborate on the data.</p>
                       <p className="banner10-p1">Learn more &nbsp; &#8594;</p>
                     </div>
                     <div className="banner10-flex2">
                     <img className="banner10-img" src={mobile} alt="No img"/>
                       <h2 className="banner10-heading">Seal the deal</h2>
                       <p className="banner10-p">Merge form responses with a custom document template to draft contracts and legal documents. Set up an e-sign workflow and get your documents signed digitally.</p>
                       <p className="banner10-p1">Learn more &nbsp; &#8594;</p>
                     </div>
                     <div className="banner10-flex3">
                     <img className="banner10-img" src={privacy} alt="No img"/>
                       <h2 className="banner10-heading">Prioritize your privacy</h2>
                       <p className="banner10-p">We are constantly evolving and adhering to strict regulations, including GDPR, to fulfill our longstanding commitment to your privacy.</p>
                       <p className="banner10-p1">Learn more &nbsp; &#8594;</p>
                     </div>
                     </div> 
                    </div>
                    <div className="banner11">
                    <p className="banner11-p">Rated as your best online form builder software</p>
                    <div className="banner11-flex">
                      <div className="banner11-box">
                     <img className="banner11-img" src={capterra} alt="No img"/>
                      </div>
                      <div className="banner11-box">
                      <img className="banner11-img2" src={trust} alt="No img"/>
                      </div>
                      <div className="banner11-box">
                      <img className="banner11-img3" src={getapp} alt="No img"/>
                      </div>
                      <div className="banner11-box">
                      <img className="banner11-img4" src={gcrowd} alt="No img"/>
                      </div>
                    </div>
                    </div>
                    <div className="banner12">
                       <img className="banner12-img" src={build} alt="No img"/> 
                    </div>
                    <div className="banner13">
                    <h1 className="banner13-h">Keep your business in form.</h1>
                    <button className="banner13-btn" >SIGN UP FOR FREE</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default HomeTwo
