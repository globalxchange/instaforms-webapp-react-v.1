import React from 'react';
import HomeNavbar from '../../header/homeNavbar/HomeNavbar';
import "./home.scss";


function Home() {
    return (
        <div>
             <div className="main-navbar">
                    <HomeNavbar />
             </div>
            <div className="home">
                <div className="home-section1">
                    <div className="banner1">
                        <div></div>
                        <div className="banner1-Text">
                            <p className="banner1-Textone">Let's Be Honest</p>
                            <p className="banner1-Texttwo">Forms Suck</p>
                        </div>
                        <div className="banner1-Buttondiv">
                            <button className="banner1-Button">Now Fill This One Out</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
