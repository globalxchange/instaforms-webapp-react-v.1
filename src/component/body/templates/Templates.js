import React from 'react';
import './templates.scss';
import HomeNavbar from '../../header/homeNavbar/HomeNavbar';
import AOS from 'aos';
import ImageOne from './images/ChevronOne.svg';
import ImageTwo from './images/ChevronTwo.svg';
import ImageThree from './images/ChevronThree.svg';
import image2One from './images/Chevron2one.svg';
import image2Two from './images/Chevron2two.svg';
import image2Three from './images/Chevron2three.svg';
import { useState, useEffect } from 'react';
function Templates() {

    const List = [{
        id: 1,
        head: 'Excited'
    },
    {
        id: 2,
        head: 'Think'
    },
    {
        id: 3,
        head: 'Spend'
    }]

    const [num, setNum] = useState(0);

    /* onWheel Scrolling*/

    // const onWheel = (e) => {
    //     if (e.deltaY > 0) {
    //         setNum(num === 0 ? List.length - 1 : num - 1);
    //     } else {
    //         setNum(num === List.length - 1 ? 0 : num + 1);
    //     }
    // }

    useEffect(() => {

    }, []);

    return (
        <>
         <div className="main-navbar">
                    <HomeNavbar />
            </div>
            <div className="template-secone">
                <div className={num === 0 ? "template-contentone" : num === 1 ? ("template-contenttwo") : "template-contentthree"}>
                    <div className="template-text" >
                        <h4>See Forms That Make Users</h4>
                        <div className="template-text-bottom fade-in">
                            <div className="first">
                                <span onClick={() => setNum(0)} className="dotone"></span>
                                <span onClick={() => setNum(1)} className="dottwo"></span>
                                <span onClick={() => setNum(2)} className="dotthree"></span>
                            </div>
                            <div className="second">
                                <h1>{List[num].head}</h1>
                            </div>
                            <div className="third">
                                <img src={num === 1 ? image2Three : ImageThree} alt='noimg' className="arrowimg3" />
                                <img src={num === 1 ? image2Two : ImageTwo} alt='noimg' className="arrowimg2" />
                                <img src={num === 1 ? image2One : ImageOne} alt='noimg' className="arrowimg1" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Templates
