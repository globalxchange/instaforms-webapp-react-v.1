import React from 'react';
import './createnew.scss';
import DashboardNavbar from '../../header/dashboardNavbar/DashboardNavbar';
import SidebarOne from '../../sidebar/sidebarOne/SidebarOne';

function CreateNew() {
    return (
        <div>
             <div className="main-navbar">
                <DashboardNavbar />
             </div>
             <div className="createnew-content">
             <div className="sidecontainer-one">
                <SidebarOne />
             </div>
             </div>
        </div>
    )
}

export default CreateNew
