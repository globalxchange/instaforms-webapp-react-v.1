import React, { useState, useEffect } from "react";
import axios from "axios";
import { login } from "../../../services/postAPIs";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useHistory } from "react-router-dom";
import Lottie from "react-lottie";
import * as animationData from "./paperpen.json";
import "./login.scss";
function Login() {
  const history = useHistory();
  const [state, setState] = useState({ email: "", password: "" });
  const [emailinput, setEmailinput] = useState(true);
  const [loading, setLoading] = useState(false);
  const myRef = React.createRef();

  useEffect(() => {
    myRef.current.focus();
  }, []);

  const handleSubmit = (e) => {
    if (e.key === "Enter") {
      setEmailinput(false);
    }
  };

  const handleSubmitPwd = (e) => {
    if (e.key === "Enter") {
      setLoading(true);
      login(state.email, state.password).then(function (res) {
        setLoading(false);

        if (res.data.status) {
          localStorage.setItem("Tokenid", res.data.idToken);
        } else {
          toast.error(res.data.message);
        }
        const headers = {
          "Content-Type": "application/json",
          Accept: "*/*",
          email: state.email,
          token: res.data.idToken,
        };

        axios
          .get("https://appapi2.apimachine.com/user", {
            headers: headers,
          })
          .then((response) => {
            if (response.data.status) {
              localStorage.setItem("userEmail", state.email);
              localStorage.setItem("idToken", res.data.idToken);
              history.replace("/dashboard");
              console.log("status", response.data.status);
            } else {
              history.replace("/postlogin");
            }
          });
      });
    }
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setState({
      ...state,
      [name]: value,
    });
  };

  let animate = null;
  animate = animationData.default;
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animate,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };
  return (
    <>
      {loading ? (
        <React.Fragment>
          <div className="loading-cont">
            <Lottie
              height={450}
              width={450}
              options={defaultOptions}
              className="loading-img"
            />
          </div>
        </React.Fragment>
      ) : (
        <div className="loginContainer">
          <div className="input-content">
            {emailinput === true ? (
              <input
                className="input w-100"
                type="email"
                name="email"
                value={state.email}
                placeholder="Enter Your Email..."
                ref={myRef}
                onKeyPress={handleSubmit}
                onChange={handleChange}
              />
            ) : (
              <input
                className="input"
                type="password"
                name="password"
                value={state.password}
                placeholder="Enter Password"
                ref={myRef}
                onKeyPress={handleSubmitPwd}
                onChange={handleChange}
              />
            )}
          </div>
          <div className="Extra-content"></div>
          <div></div>
          <ToastContainer />
        </div>
      )}
    </>
  );
}

export default Login;
