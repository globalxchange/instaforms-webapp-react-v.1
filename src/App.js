import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.scss';
import Home from '../src/component/body/home/Home';
import HomeTwo from '../src/component/body/hometwo/HomeTwo';
import Templates from '../src/component/body/templates/Templates';
import Login from '../src/component/body/login/Login';
import Dashboard from '../src/component/body/dashboard/Dashboard';
import Market from '../src/component/body/market/Market';
import Data from '../src/component/body/data/Data';
import Integrations from '../src/component/body/integrations/Integrations';
import CreateNew from '../src/component/body/createNew/CreateNew';
import PostRegistration from '../src/component/body/registration/PostRegistration';

function App() {
  return (
    <div className="main-container">
      {/* <div className="main-navbar">
        <HomeNavbar />
      </div> */}
      <div className="main-content">
        <Switch>
          <Route path="/integrations" component={Integrations} />
          <Route path="/data" component={Data} />
          <Route path="/market" component={Market} />
          <Route path="/dashboard/createnew" component={CreateNew} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path='/postlogin' component={PostRegistration} />
          <Route path="/login" component={Login} />
          <Route path="/templates" component={Templates} />
          <Route path="/home2" component={HomeTwo} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
