import axios from 'axios';

export const authenticate = () =>{
    let email = localStorage.getItem('userEmail');
    let token = localStorage.getItem('idToken');

    return axios.post(`https://comms.globalxchange.com/coin/verifyToken`,
        {email: email, token: token})
};

export const login =(email, password) =>{
    console.log("login 1", email, password)
    return axios.post(`https://gxauth.apimachine.com/gx/user/login`,{
        email: email, password: password
    })
};